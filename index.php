<?php
try {   
    $stdout = fopen('php://stdout', 'w');
    
    print("HOSTNAME: ". gethostname() . " \n v1 ");

    fwrite($stdout, json_encode($_ENV, JSON_PRETTY_PRINT));
    fclose($stdout);
} catch (Exception $e) {
    header("HTTP/1.1 503"); 
    print($e->getMessage());
} 
?>
