FROM php:7.4-apache
RUN echo "Listen 5000" > /etc/apache2/ports.conf
COPY . /var/www/html/
EXPOSE 5000
